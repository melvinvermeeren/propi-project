﻿USE [iproject35];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'antiquariat-theophil', N'rabobank', N'121436685', N'post', NULL UNION ALL
SELECT N'stampnext2013', N'rabobank', N'121436698', N'post', NULL UNION ALL
SELECT N'incagold2011', N'rabobank', N'121436711', N'post', NULL UNION ALL
SELECT N'f0reverc0llect', N'rabobank', N'121436724', N'post', NULL UNION ALL
SELECT N'simonisspoelder', N'rabobank', N'121436737', N'post', NULL UNION ALL
SELECT N'stamp68', N'rabobank', N'121436750', N'post', NULL UNION ALL
SELECT N'orphean78', N'rabobank', N'121436763', N'post', NULL UNION ALL
SELECT N'warenplanet', N'rabobank', N'121436776', N'post', NULL UNION ALL
SELECT N'jos8311', N'rabobank', N'121436789', N'post', NULL UNION ALL
SELECT N'djl4007', N'rabobank', N'121436802', N'post', NULL UNION ALL
SELECT N'bulba_store_cards', N'rabobank', N'121436815', N'post', NULL UNION ALL
SELECT N'magicwell6804', N'rabobank', N'121436828', N'post', NULL UNION ALL
SELECT N'edrott', N'rabobank', N'121436841', N'post', NULL UNION ALL
SELECT N'kooky_toys_and_baby_kooky', N'rabobank', N'121436854', N'post', NULL UNION ALL
SELECT N'cocoon-hd', N'rabobank', N'121436867', N'post', NULL UNION ALL
SELECT N'jackyottenheijm', N'rabobank', N'121436880', N'post', NULL UNION ALL
SELECT N'tinamarie881', N'rabobank', N'121436893', N'post', NULL UNION ALL
SELECT N'englishendeavour', N'rabobank', N'121436906', N'post', NULL UNION ALL
SELECT N'angel-touch04', N'rabobank', N'121436919', N'post', NULL UNION ALL
SELECT N'chenduz', N'rabobank', N'121436932', N'post', NULL UNION ALL
SELECT N'vwacc', N'rabobank', N'121436945', N'post', NULL UNION ALL
SELECT N'www_lopo24_de', N'rabobank', N'121436958', N'post', NULL UNION ALL
SELECT N'bebopmathias', N'rabobank', N'121436971', N'post', NULL UNION ALL
SELECT N'briefmarken-werner', N'rabobank', N'121436984', N'post', NULL UNION ALL
SELECT N'abeldoorn', N'rabobank', N'121436997', N'post', NULL UNION ALL
SELECT N'thon-buch', N'rabobank', N'121437010', N'post', NULL UNION ALL
SELECT N'andrewrichmond', N'rabobank', N'121437023', N'post', NULL UNION ALL
SELECT N'haveanicedaytoday', N'rabobank', N'121437036', N'post', NULL UNION ALL
SELECT N'collezionandoroma', N'rabobank', N'121437049', N'post', NULL UNION ALL
SELECT N'deschaele', N'rabobank', N'121437062', N'post', NULL UNION ALL
SELECT N'viooltje1', N'rabobank', N'121437075', N'post', NULL UNION ALL
SELECT N'cardexchange-com', N'rabobank', N'121437088', N'post', NULL UNION ALL
SELECT N'karten-gandalf', N'rabobank', N'121437101', N'post', NULL UNION ALL
SELECT N'jump.store', N'rabobank', N'121437114', N'post', NULL UNION ALL
SELECT N'zoeker_123', N'rabobank', N'121437127', N'post', NULL UNION ALL
SELECT N'biklam', N'rabobank', N'121437140', N'post', NULL UNION ALL
SELECT N'jodokus57', N'rabobank', N'121437153', N'post', NULL UNION ALL
SELECT N'vivambaradam59', N'rabobank', N'121437166', N'post', NULL UNION ALL
SELECT N'hollandsales', N'rabobank', N'121437179', N'post', NULL UNION ALL
SELECT N'pim1954', N'rabobank', N'121437192', N'post', NULL UNION ALL
SELECT N'greatpapertreasure', N'rabobank', N'121437205', N'post', NULL UNION ALL
SELECT N'oxmouth', N'rabobank', N'121437218', N'post', NULL UNION ALL
SELECT N'hotfashionlove', N'rabobank', N'121437231', N'post', NULL UNION ALL
SELECT N'1904tom', N'rabobank', N'121437244', N'post', NULL UNION ALL
SELECT N'mrbrave1', N'rabobank', N'121437257', N'post', NULL UNION ALL
SELECT N'silvershop1962', N'rabobank', N'121437270', N'post', NULL UNION ALL
SELECT N'ackrings', N'rabobank', N'121437283', N'post', NULL UNION ALL
SELECT N'toost44', N'rabobank', N'121437296', N'post', NULL UNION ALL
SELECT N'wilstra', N'rabobank', N'121437309', N'post', NULL UNION ALL
SELECT N'drroem', N'rabobank', N'121437322', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'buntespapier_de', N'rabobank', N'121437335', N'post', NULL UNION ALL
SELECT N'pokemon-trading-cards', N'rabobank', N'121437348', N'post', NULL UNION ALL
SELECT N'8109monique', N'rabobank', N'121437361', N'post', NULL UNION ALL
SELECT N'1technofriek', N'rabobank', N'121437374', N'post', NULL UNION ALL
SELECT N'docholiday0815', N'rabobank', N'121437387', N'post', NULL UNION ALL
SELECT N'davidcharliebrown', N'rabobank', N'121437400', N'post', NULL UNION ALL
SELECT N'haansberg', N'rabobank', N'121437413', N'post', NULL UNION ALL
SELECT N'dutchphilatelist', N'rabobank', N'121437426', N'post', NULL UNION ALL
SELECT N'pubro', N'rabobank', N'121437439', N'post', NULL UNION ALL
SELECT N'ccsims02', N'rabobank', N'121437452', N'post', NULL UNION ALL
SELECT N'hjr1979...0', N'rabobank', N'121437465', N'post', NULL UNION ALL
SELECT N'tiju-markt', N'rabobank', N'121437478', N'post', NULL UNION ALL
SELECT N'meerrauschen', N'rabobank', N'121437491', N'post', NULL UNION ALL
SELECT N'boompje', N'rabobank', N'121437504', N'post', NULL UNION ALL
SELECT N'madformags', N'rabobank', N'121437517', N'post', NULL UNION ALL
SELECT N'wimwoning', N'rabobank', N'121437530', N'post', NULL UNION ALL
SELECT N'carrillio', N'rabobank', N'121437543', N'post', NULL UNION ALL
SELECT N'yorgos43', N'rabobank', N'121437556', N'post', NULL UNION ALL
SELECT N'nostalgie-piet', N'rabobank', N'121437569', N'post', NULL UNION ALL
SELECT N'comrola', N'rabobank', N'121437582', N'post', NULL UNION ALL
SELECT N'plunduff', N'rabobank', N'121437595', N'post', NULL UNION ALL
SELECT N'antikundpostkartenfund', N'rabobank', N'121437608', N'post', NULL UNION ALL
SELECT N'truke1738', N'rabobank', N'121437621', N'post', NULL UNION ALL
SELECT N'paulysre', N'rabobank', N'121437634', N'post', NULL UNION ALL
SELECT N'buch-favoriten', N'rabobank', N'121437647', N'post', NULL UNION ALL
SELECT N'flippero13', N'rabobank', N'121437660', N'post', NULL UNION ALL
SELECT N'cheappromostuff', N'rabobank', N'121437673', N'post', NULL UNION ALL
SELECT N'iahm-h', N'rabobank', N'121437686', N'post', NULL UNION ALL
SELECT N'cheap.monkey', N'rabobank', N'121437699', N'post', NULL UNION ALL
SELECT N'007hannah', N'rabobank', N'121437712', N'post', NULL UNION ALL
SELECT N'corur', N'rabobank', N'121437725', N'post', NULL UNION ALL
SELECT N'fortuyn79', N'rabobank', N'121437738', N'post', NULL UNION ALL
SELECT N'hemelrijk', N'rabobank', N'121437751', N'post', NULL UNION ALL
SELECT N'mooiweertje22', N'rabobank', N'121437764', N'post', NULL UNION ALL
SELECT N'northwinds', N'rabobank', N'121437777', N'post', NULL UNION ALL
SELECT N'javanes100', N'rabobank', N'121437790', N'post', NULL UNION ALL
SELECT N'sellabella2000', N'rabobank', N'121437803', N'post', NULL UNION ALL
SELECT N'wwwgebrauchtpcde', N'rabobank', N'121437816', N'post', NULL UNION ALL
SELECT N'uptown_girl4ever', N'rabobank', N'121437829', N'post', NULL UNION ALL
SELECT N'langelat', N'rabobank', N'121437842', N'post', NULL UNION ALL
SELECT N'trader-online-de', N'rabobank', N'121437855', N'post', NULL UNION ALL
SELECT N'bos.cards', N'rabobank', N'121437868', N'post', NULL UNION ALL
SELECT N'verzamelaar1968', N'rabobank', N'121437881', N'post', NULL UNION ALL
SELECT N'bertbask97', N'rabobank', N'121437894', N'post', NULL UNION ALL
SELECT N'admul', N'rabobank', N'121437907', N'post', NULL UNION ALL
SELECT N'casandro99', N'rabobank', N'121437920', N'post', NULL UNION ALL
SELECT N'rozewoodstock', N'rabobank', N'121437933', N'post', NULL UNION ALL
SELECT N'buera', N'rabobank', N'121437946', N'post', NULL UNION ALL
SELECT N'teundereede', N'rabobank', N'121437959', N'post', NULL UNION ALL
SELECT N'arthur6339', N'rabobank', N'121437972', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'nena-dejong2', N'rabobank', N'121437985', N'post', NULL UNION ALL
SELECT N'haja-1', N'rabobank', N'121437998', N'post', NULL UNION ALL
SELECT N'engels-onlineshop-2008', N'rabobank', N'121438011', N'post', NULL UNION ALL
SELECT N'decazo1', N'rabobank', N'121438024', N'post', NULL UNION ALL
SELECT N'mgh-medienverpackungen', N'rabobank', N'121438037', N'post', NULL UNION ALL
SELECT N'zeppelin.', N'rabobank', N'121438050', N'post', NULL UNION ALL
SELECT N'tolboek1651', N'rabobank', N'121438063', N'post', NULL UNION ALL
SELECT N'giannifrats', N'rabobank', N'121438076', N'post', NULL UNION ALL
SELECT N'mustbebonkers', N'rabobank', N'121438089', N'post', NULL UNION ALL
SELECT N'musicsubway', N'rabobank', N'121438102', N'post', NULL UNION ALL
SELECT N'milvg11', N'rabobank', N'121438115', N'post', NULL UNION ALL
SELECT N'1980-lions-toys', N'rabobank', N'121438128', N'post', NULL UNION ALL
SELECT N'jp-stamps', N'rabobank', N'121438141', N'post', NULL UNION ALL
SELECT N'myphila1', N'rabobank', N'121438154', N'post', NULL UNION ALL
SELECT N'stock-optics', N'rabobank', N'121438167', N'post', NULL UNION ALL
SELECT N'vlubico', N'rabobank', N'121438180', N'post', NULL UNION ALL
SELECT N'jessy1200', N'rabobank', N'121438193', N'post', NULL UNION ALL
SELECT N'istantineltempo', N'rabobank', N'121438206', N'post', NULL UNION ALL
SELECT N'arrowsjunior', N'rabobank', N'121438219', N'post', NULL UNION ALL
SELECT N'gengi2006', N'rabobank', N'121438232', N'post', NULL UNION ALL
SELECT N'poujol54', N'rabobank', N'121438245', N'post', NULL UNION ALL
SELECT N'thegoodtapir1967', N'rabobank', N'121438258', N'post', NULL UNION ALL
SELECT N'230v-berlin', N'rabobank', N'121438271', N'post', NULL UNION ALL
SELECT N'sammleroasesauerug', N'rabobank', N'121438284', N'post', NULL UNION ALL
SELECT N'mo-schu', N'rabobank', N'121438297', N'post', NULL UNION ALL
SELECT N'stonedhenge100', N'rabobank', N'121438310', N'post', NULL UNION ALL
SELECT N'white-eagle2001', N'rabobank', N'121438323', N'post', NULL UNION ALL
SELECT N'halcyonbooksuk', N'rabobank', N'121438336', N'post', NULL UNION ALL
SELECT N'cosworth46', N'rabobank', N'121438349', N'post', NULL UNION ALL
SELECT N'voigt9532', N'rabobank', N'121438362', N'post', NULL UNION ALL
SELECT N'marypoppinsbag', N'rabobank', N'121438375', N'post', NULL UNION ALL
SELECT N'wolters291154', N'rabobank', N'121438388', N'post', NULL UNION ALL
SELECT N'voordeel-man', N'rabobank', N'121438401', N'post', NULL UNION ALL
SELECT N'thecollectorzone', N'rabobank', N'121438414', N'post', NULL UNION ALL
SELECT N'zeitsprung-klassiker', N'rabobank', N'121438427', N'post', NULL UNION ALL
SELECT N'yugi-oase', N'rabobank', N'121438440', N'post', NULL UNION ALL
SELECT N'orcawal-berlin', N'rabobank', N'121438453', N'post', NULL UNION ALL
SELECT N'2007rutger', N'rabobank', N'121438466', N'post', NULL UNION ALL
SELECT N'19abp40', N'rabobank', N'121438479', N'post', NULL UNION ALL
SELECT N'sauliusst', N'rabobank', N'121438492', N'post', NULL UNION ALL
SELECT N'posingbabybeau', N'rabobank', N'121438505', N'post', NULL UNION ALL
SELECT N'bonfire.2007', N'rabobank', N'121438518', N'post', NULL UNION ALL
SELECT N'divando2005', N'rabobank', N'121438531', N'post', NULL UNION ALL
SELECT N'cerenau', N'rabobank', N'121438544', N'post', NULL UNION ALL
SELECT N'keesmarklin', N'rabobank', N'121438557', N'post', NULL UNION ALL
SELECT N'kappie_rob', N'rabobank', N'121438570', N'post', NULL UNION ALL
SELECT N'booksncomics', N'rabobank', N'121438583', N'post', NULL UNION ALL
SELECT N'spielundhobbybrauns', N'rabobank', N'121438596', N'post', NULL UNION ALL
SELECT N'hennie8018', N'rabobank', N'121438609', N'post', NULL UNION ALL
SELECT N'wit1300', N'rabobank', N'121438622', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'www-figuren-shop-de', N'rabobank', N'121438635', N'post', NULL UNION ALL
SELECT N'1comicworld', N'rabobank', N'121438648', N'post', NULL UNION ALL
SELECT N'jorhanstamps', N'rabobank', N'121438661', N'post', NULL UNION ALL
SELECT N'americo1970', N'rabobank', N'121438674', N'post', NULL UNION ALL
SELECT N'ccs-74', N'rabobank', N'121438687', N'post', NULL UNION ALL
SELECT N'woody_simmy', N'rabobank', N'121438700', N'post', NULL UNION ALL
SELECT N'antiktharandt09', N'rabobank', N'121438713', N'post', NULL UNION ALL
SELECT N'ladyjinxx', N'rabobank', N'121438726', N'post', NULL UNION ALL
SELECT N'xander8835', N'rabobank', N'121438739', N'post', NULL UNION ALL
SELECT N'005bond', N'rabobank', N'121438752', N'post', NULL UNION ALL
SELECT N'yuuretsu', N'rabobank', N'121438765', N'post', NULL UNION ALL
SELECT N'golfpurzelmaus', N'rabobank', N'121438778', N'post', NULL UNION ALL
SELECT N'orbitingbooks_online', N'rabobank', N'121438791', N'post', NULL UNION ALL
SELECT N'ha0152', N'rabobank', N'121438804', N'post', NULL UNION ALL
SELECT N'books_discount', N'rabobank', N'121438817', N'post', NULL UNION ALL
SELECT N'figurensammelsurium', N'rabobank', N'121438830', N'post', NULL UNION ALL
SELECT N'bischofantik', N'rabobank', N'121438843', N'post', NULL UNION ALL
SELECT N'clubit_tv', N'rabobank', N'121438856', N'post', NULL UNION ALL
SELECT N'elly1304elly', N'rabobank', N'121438869', N'post', NULL UNION ALL
SELECT N'dwergke77', N'rabobank', N'121438882', N'post', NULL UNION ALL
SELECT N'sammelartikel09', N'rabobank', N'121438895', N'post', NULL UNION ALL
SELECT N'janssenhuitema', N'rabobank', N'121438908', N'post', NULL UNION ALL
SELECT N'np4amsterdam', N'rabobank', N'121438921', N'post', NULL UNION ALL
SELECT N'robbie-39', N'rabobank', N'121438934', N'post', NULL UNION ALL
SELECT N'bikerhelmetholland', N'rabobank', N'121438947', N'post', NULL UNION ALL
SELECT N'cv-9', N'rabobank', N'121438960', N'post', NULL UNION ALL
SELECT N'audiator', N'rabobank', N'121438973', N'post', NULL UNION ALL
SELECT N'rote_irma', N'rabobank', N'121438986', N'post', NULL UNION ALL
SELECT N'pietjacobijn', N'rabobank', N'121438999', N'post', NULL UNION ALL
SELECT N'*sammelsurium*2010', N'rabobank', N'121439012', N'post', NULL UNION ALL
SELECT N'tobe0008', N'rabobank', N'121439025', N'post', NULL UNION ALL
SELECT N'jastrzebieslough', N'rabobank', N'121439038', N'post', NULL UNION ALL
SELECT N'pmpewter2009', N'rabobank', N'121439051', N'post', NULL UNION ALL
SELECT N'hh-automobilia', N'rabobank', N'121439064', N'post', NULL UNION ALL
SELECT N'zursammlereckeba', N'rabobank', N'121439077', N'post', NULL UNION ALL
SELECT N'redhotcomics', N'rabobank', N'121439090', N'post', NULL UNION ALL
SELECT N'hero4sale', N'rabobank', N'121439103', N'post', NULL UNION ALL
SELECT N'boekenplaat', N'rabobank', N'121439116', N'post', NULL UNION ALL
SELECT N'alfcoin', N'rabobank', N'121439129', N'post', NULL UNION ALL
SELECT N'a-place-in-space', N'rabobank', N'121439142', N'post', NULL UNION ALL
SELECT N'blackermoon', N'rabobank', N'121439155', N'post', NULL UNION ALL
SELECT N'vliegtuigjack123', N'rabobank', N'121439168', N'post', NULL UNION ALL
SELECT N'zinkes-sammelwelt', N'rabobank', N'121439181', N'post', NULL UNION ALL
SELECT N'mgg1000', N'rabobank', N'121439194', N'post', NULL UNION ALL
SELECT N'illygirl', N'rabobank', N'121439207', N'post', NULL UNION ALL
SELECT N'antiquariatedelalt', N'rabobank', N'121439220', N'post', NULL UNION ALL
SELECT N'delta98holland', N'rabobank', N'121439233', N'post', NULL UNION ALL
SELECT N'cokeglas', N'rabobank', N'121439246', N'post', NULL UNION ALL
SELECT N'la.siesta', N'rabobank', N'121439259', N'post', NULL UNION ALL
SELECT N'vanges', N'rabobank', N'121439272', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N're71', N'rabobank', N'121439285', N'post', NULL UNION ALL
SELECT N'dedichter84', N'rabobank', N'121439298', N'post', NULL UNION ALL
SELECT N'haddocks_nl', N'rabobank', N'121439311', N'post', NULL UNION ALL
SELECT N'*jimsairauctions.com*', N'rabobank', N'121439324', N'post', NULL UNION ALL
SELECT N'vintage-things-forever', N'rabobank', N'121439337', N'post', NULL UNION ALL
SELECT N'ronnerii', N'rabobank', N'121439350', N'post', NULL UNION ALL
SELECT N'bart.12', N'rabobank', N'121439363', N'post', NULL UNION ALL
SELECT N'euroverzend', N'rabobank', N'121439376', N'post', NULL UNION ALL
SELECT N'henkterbraak', N'rabobank', N'121439389', N'post', NULL UNION ALL
SELECT N'sonntagskind24', N'rabobank', N'121439402', N'post', NULL UNION ALL
SELECT N'rob.stamps', N'rabobank', N'121439415', N'post', NULL UNION ALL
SELECT N'grotusque', N'rabobank', N'121439428', N'post', NULL UNION ALL
SELECT N'martinhk1958', N'rabobank', N'121439441', N'post', NULL UNION ALL
SELECT N'willowroxana', N'rabobank', N'121439454', N'post', NULL UNION ALL
SELECT N'marinusvanriel', N'rabobank', N'121439467', N'post', NULL UNION ALL
SELECT N'aqua7000', N'rabobank', N'121439480', N'post', NULL UNION ALL
SELECT N'2012louisprima', N'rabobank', N'121439493', N'post', NULL UNION ALL
SELECT N'da*gecko', N'rabobank', N'121439506', N'post', NULL UNION ALL
SELECT N'jaws2104', N'rabobank', N'121439519', N'post', NULL UNION ALL
SELECT N'six-years', N'rabobank', N'121439532', N'post', NULL UNION ALL
SELECT N'goldrhox', N'rabobank', N'121439545', N'post', NULL UNION ALL
SELECT N'charly-c', N'rabobank', N'121439558', N'post', NULL UNION ALL
SELECT N'1807amy', N'rabobank', N'121439571', N'post', NULL UNION ALL
SELECT N'colorofmagic2012', N'rabobank', N'121439584', N'post', NULL UNION ALL
SELECT N'erikyvan', N'rabobank', N'121439597', N'post', NULL UNION ALL
SELECT N'sammelkrams', N'rabobank', N'121439610', N'post', NULL UNION ALL
SELECT N'nynaga90', N'rabobank', N'121439623', N'post', NULL UNION ALL
SELECT N'ostfriesischer-sammlertreff', N'rabobank', N'121439636', N'post', NULL UNION ALL
SELECT N'pvl74', N'rabobank', N'121439649', N'post', NULL UNION ALL
SELECT N'figuren-shop-kulturen', N'rabobank', N'121439662', N'post', NULL UNION ALL
SELECT N'verzamelkaarten', N'rabobank', N'121439675', N'post', NULL UNION ALL
SELECT N'sejacru', N'rabobank', N'121439688', N'post', NULL UNION ALL
SELECT N'robgofast', N'rabobank', N'121439701', N'post', NULL UNION ALL
SELECT N'etalagevonkie', N'rabobank', N'121439714', N'post', NULL UNION ALL
SELECT N'amyluke46', N'rabobank', N'121439727', N'post', NULL UNION ALL
SELECT N'ozz_monster', N'rabobank', N'121439740', N'post', NULL UNION ALL
SELECT N'bdk-coin-and-bookstore', N'rabobank', N'121439753', N'post', NULL UNION ALL
SELECT N'littlebigbook', N'rabobank', N'121439766', N'post', NULL UNION ALL
SELECT N'bastiaan187', N'rabobank', N'121439779', N'post', NULL UNION ALL
SELECT N'greaserwold', N'rabobank', N'121439792', N'post', NULL UNION ALL
SELECT N'bookfreaknl', N'rabobank', N'121439805', N'post', NULL UNION ALL
SELECT N'patjng09', N'rabobank', N'121439818', N'post', NULL UNION ALL
SELECT N'fast_collectables777', N'rabobank', N'121439831', N'post', NULL UNION ALL
SELECT N'alfonsclemens', N'rabobank', N'121439844', N'post', NULL UNION ALL
SELECT N'stamm-antik', N'rabobank', N'121439857', N'post', NULL UNION ALL
SELECT N'unikum1957', N'rabobank', N'121439870', N'post', NULL UNION ALL
SELECT N'2004plien', N'rabobank', N'121439883', N'post', NULL UNION ALL
SELECT N'sam_in_barbate', N'rabobank', N'121439896', N'post', NULL UNION ALL
SELECT N'saitou_tokio', N'rabobank', N'121439909', N'post', NULL UNION ALL
SELECT N'hup.vosdaal4', N'rabobank', N'121439922', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'holm-seppenser', N'rabobank', N'121439935', N'post', NULL UNION ALL
SELECT N'vieleangebote1', N'rabobank', N'121439948', N'post', NULL UNION ALL
SELECT N'soesazelf', N'rabobank', N'121439961', N'post', NULL UNION ALL
SELECT N'lightningbolt1979', N'rabobank', N'121439974', N'post', NULL UNION ALL
SELECT N'ro-militaria', N'rabobank', N'121439987', N'post', NULL UNION ALL
SELECT N'twilight-zone-dordrecht', N'rabobank', N'121440000', N'post', NULL UNION ALL
SELECT N'iris50iris', N'rabobank', N'121440013', N'post', NULL UNION ALL
SELECT N'spacedragonx', N'rabobank', N'121440026', N'post', NULL UNION ALL
SELECT N'skysmit', N'rabobank', N'121440039', N'post', NULL UNION ALL
SELECT N'wennekes2005', N'rabobank', N'121440052', N'post', NULL UNION ALL
SELECT N'bauer4723', N'rabobank', N'121440065', N'post', NULL UNION ALL
SELECT N'nikki2010live', N'rabobank', N'121440078', N'post', NULL UNION ALL
SELECT N'toy-company-1959', N'rabobank', N'121440091', N'post', NULL UNION ALL
SELECT N'phoenixtoyshop', N'rabobank', N'121440104', N'post', NULL UNION ALL
SELECT N'alexvictor123', N'rabobank', N'121440117', N'post', NULL UNION ALL
SELECT N'orbo-antik', N'rabobank', N'121440130', N'post', NULL UNION ALL
SELECT N'emmy-sero', N'rabobank', N'121440143', N'post', NULL UNION ALL
SELECT N'eklo2002', N'rabobank', N'121440156', N'post', NULL UNION ALL
SELECT N'24musiclady', N'rabobank', N'121440169', N'post', NULL UNION ALL
SELECT N'belfi1955', N'rabobank', N'121440182', N'post', NULL UNION ALL
SELECT N'amsteltoren', N'rabobank', N'121440195', N'post', NULL UNION ALL
SELECT N'rene10nl', N'rabobank', N'121440208', N'post', NULL UNION ALL
SELECT N'yoshis_collectibles', N'rabobank', N'121440221', N'post', NULL UNION ALL
SELECT N'ernstvanderploeg', N'rabobank', N'121440234', N'post', NULL UNION ALL
SELECT N'facta_non_verba010', N'rabobank', N'121440247', N'post', NULL UNION ALL
SELECT N'guardion', N'rabobank', N'121440260', N'post', NULL UNION ALL
SELECT N'bonnjorno', N'rabobank', N'121440273', N'post', NULL UNION ALL
SELECT N'bikersstore', N'rabobank', N'121440286', N'post', NULL UNION ALL
SELECT N'spielzeug-kabinett', N'rabobank', N'121440299', N'post', NULL UNION ALL
SELECT N'nielsklinkenberg', N'rabobank', N'121440312', N'post', NULL UNION ALL
SELECT N'coldtonnageman', N'rabobank', N'121440325', N'post', NULL UNION ALL
SELECT N'disney-club', N'rabobank', N'121440338', N'post', NULL UNION ALL
SELECT N'balak_nl', N'rabobank', N'121440351', N'post', NULL UNION ALL
SELECT N'theantiquesstorehouse', N'rabobank', N'121440364', N'post', NULL UNION ALL
SELECT N'preetz1947', N'rabobank', N'121440377', N'post', NULL UNION ALL
SELECT N'beutebucht-de', N'rabobank', N'121440390', N'post', NULL UNION ALL
SELECT N'harmpie22', N'rabobank', N'121440403', N'post', NULL UNION ALL
SELECT N'dassrockt', N'rabobank', N'121440416', N'post', NULL UNION ALL
SELECT N'buchhoffmanneutin', N'rabobank', N'121440429', N'post', NULL UNION ALL
SELECT N'nici-store', N'rabobank', N'121440442', N'post', NULL UNION ALL
SELECT N'gandalfur354', N'rabobank', N'121440455', N'post', NULL UNION ALL
SELECT N'johns-modelcarsandtrains', N'rabobank', N'121440468', N'post', NULL UNION ALL
SELECT N'dindabumba', N'rabobank', N'121440481', N'post', NULL UNION ALL
SELECT N'angi7778', N'rabobank', N'121440494', N'post', NULL UNION ALL
SELECT N'reuring', N'rabobank', N'121440507', N'post', NULL UNION ALL
SELECT N'helleus', N'rabobank', N'121440520', N'post', NULL UNION ALL
SELECT N'alex-1965', N'rabobank', N'121440533', N'post', NULL UNION ALL
SELECT N'ingrids-geschenke-truhe', N'rabobank', N'121440546', N'post', NULL UNION ALL
SELECT N'thehouseofcomics', N'rabobank', N'121440559', N'post', NULL UNION ALL
SELECT N'pcpostcards', N'rabobank', N'121440572', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 6.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'thethreeneils222', N'rabobank', N'121440585', N'post', NULL UNION ALL
SELECT N'floka234', N'rabobank', N'121440598', N'post', NULL UNION ALL
SELECT N'heup1', N'rabobank', N'121440611', N'post', NULL UNION ALL
SELECT N'aramblauw', N'rabobank', N'121440624', N'post', NULL UNION ALL
SELECT N'dickenester', N'rabobank', N'121440637', N'post', NULL UNION ALL
SELECT N'antikteam', N'rabobank', N'121440650', N'post', NULL UNION ALL
SELECT N'tisjau', N'rabobank', N'121440663', N'post', NULL UNION ALL
SELECT N'buch-nord', N'rabobank', N'121440676', N'post', NULL UNION ALL
SELECT N'fcdisie', N'rabobank', N'121440689', N'post', NULL UNION ALL
SELECT N'stampdealers', N'rabobank', N'121440702', N'post', NULL UNION ALL
SELECT N'h2comics', N'rabobank', N'121440715', N'post', NULL UNION ALL
SELECT N'philon.berlin', N'rabobank', N'121440728', N'post', NULL UNION ALL
SELECT N'jawajunkyard', N'rabobank', N'121440741', N'post', NULL UNION ALL
SELECT N'alternativkauf', N'rabobank', N'121440754', N'post', NULL UNION ALL
SELECT N'g-dorfer', N'rabobank', N'121440767', N'post', NULL UNION ALL
SELECT N'trijpvanr', N'rabobank', N'121440780', N'post', NULL UNION ALL
SELECT N'sky-toys25', N'rabobank', N'121440793', N'post', NULL UNION ALL
SELECT N'hollandsale', N'rabobank', N'121440806', N'post', NULL UNION ALL
SELECT N'm.den.hartog', N'rabobank', N'121440819', N'post', NULL UNION ALL
SELECT N'wvd-verzamelen', N'rabobank', N'121440832', N'post', NULL UNION ALL
SELECT N'sandernr1', N'rabobank', N'121440845', N'post', NULL UNION ALL
SELECT N'robcar30', N'rabobank', N'121440858', N'post', NULL UNION ALL
SELECT N'snaucke', N'rabobank', N'121440871', N'post', NULL UNION ALL
SELECT N'ronniescholten', N'rabobank', N'121440884', N'post', NULL UNION ALL
SELECT N'jeroen313', N'rabobank', N'121440897', N'post', NULL UNION ALL
SELECT N'verzamelaar110', N'rabobank', N'121440910', N'post', NULL UNION ALL
SELECT N'wolf-antik', N'rabobank', N'121440923', N'post', NULL UNION ALL
SELECT N'dinkylou2', N'rabobank', N'121440936', N'post', NULL UNION ALL
SELECT N'koopveiligonlineshop', N'rabobank', N'121440949', N'post', NULL UNION ALL
SELECT N'mr.koelman', N'rabobank', N'121440962', N'post', NULL UNION ALL
SELECT N'petersmuziek', N'rabobank', N'121440975', N'post', NULL UNION ALL
SELECT N'poekiebeertje26', N'rabobank', N'121440988', N'post', NULL UNION ALL
SELECT N'2012-mustang', N'rabobank', N'121441001', N'post', NULL UNION ALL
SELECT N'moetek-trading', N'rabobank', N'121441014', N'post', NULL UNION ALL
SELECT N't*gun', N'rabobank', N'121441027', N'post', NULL UNION ALL
SELECT N'in*a', N'rabobank', N'121441040', N'post', NULL UNION ALL
SELECT N'jon30v12', N'rabobank', N'121441053', N'post', NULL UNION ALL
SELECT N'kunstbuch-shop_de', N'rabobank', N'121441066', N'post', NULL UNION ALL
SELECT N'soundconnexion', N'rabobank', N'121441079', N'post', NULL UNION ALL
SELECT N'rilela', N'rabobank', N'121441092', N'post', NULL UNION ALL
SELECT N'mjwa1212', N'rabobank', N'121441105', N'post', NULL UNION ALL
SELECT N'antiqueprintsandmaps_nl', N'rabobank', N'121441118', N'post', NULL UNION ALL
SELECT N'indiana3366', N'rabobank', N'121441131', N'post', NULL UNION ALL
SELECT N'tabletop-miniatures.de.vu', N'rabobank', N'121441144', N'post', NULL UNION ALL
SELECT N'opgeruimder', N'rabobank', N'121441157', N'post', NULL UNION ALL
SELECT N'angelique1548', N'rabobank', N'121441170', N'post', NULL UNION ALL
SELECT N'galerieurbanphotos', N'rabobank', N'121441183', N'post', NULL UNION ALL
SELECT N'spaansespook', N'rabobank', N'121441196', N'post', NULL UNION ALL
SELECT N'madro0012012', N'rabobank', N'121441209', N'post', NULL UNION ALL
SELECT N'kever1965', N'rabobank', N'121441222', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 7.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'radioalt', N'rabobank', N'121441235', N'post', NULL UNION ALL
SELECT N'suus78', N'rabobank', N'121441248', N'post', NULL UNION ALL
SELECT N'general-trust', N'rabobank', N'121441261', N'post', NULL UNION ALL
SELECT N'gucci_1976', N'rabobank', N'121441274', N'post', NULL UNION ALL
SELECT N'chinchon', N'rabobank', N'121441287', N'post', NULL UNION ALL
SELECT N'helldc', N'rabobank', N'121441300', N'post', NULL UNION ALL
SELECT N'the_sassafrass_store', N'rabobank', N'121441313', N'post', NULL UNION ALL
SELECT N'26-toppertje', N'rabobank', N'121441326', N'post', NULL UNION ALL
SELECT N'matrix3476', N'rabobank', N'121441339', N'post', NULL UNION ALL
SELECT N'tb_store', N'rabobank', N'121441352', N'post', NULL UNION ALL
SELECT N'coued', N'rabobank', N'121441365', N'post', NULL UNION ALL
SELECT N'1more-4you', N'rabobank', N'121441378', N'post', NULL UNION ALL
SELECT N'petersenboeken', N'rabobank', N'121441391', N'post', NULL UNION ALL
SELECT N'shake1860', N'rabobank', N'121441404', N'post', NULL UNION ALL
SELECT N'joergszdresden', N'rabobank', N'121441417', N'post', NULL UNION ALL
SELECT N'piedijk', N'rabobank', N'121441430', N'post', NULL UNION ALL
SELECT N'9360eugene', N'rabobank', N'121441443', N'post', NULL UNION ALL
SELECT N'fff-foto', N'rabobank', N'121441456', N'post', NULL UNION ALL
SELECT N'pkowsepp', N'rabobank', N'121441469', N'post', NULL UNION ALL
SELECT N'mangaboygoku', N'rabobank', N'121441482', N'post', NULL UNION ALL
SELECT N'anreiner.2417', N'rabobank', N'121441495', N'post', NULL UNION ALL
SELECT N'antikspielzeug', N'rabobank', N'121441508', N'post', NULL UNION ALL
SELECT N'lyka_nl', N'rabobank', N'121441521', N'post', NULL UNION ALL
SELECT N'remi11_nl', N'rabobank', N'121441534', N'post', NULL UNION ALL
SELECT N'lizmw', N'rabobank', N'121441547', N'post', NULL UNION ALL
SELECT N'raer1547', N'rabobank', N'121441560', N'post', NULL UNION ALL
SELECT N'manime_110', N'rabobank', N'121441573', N'post', NULL UNION ALL
SELECT N'olesyaw', N'rabobank', N'121441586', N'post', NULL UNION ALL
SELECT N'koekselstaafje', N'rabobank', N'121441599', N'post', NULL UNION ALL
SELECT N'bierpaleis', N'rabobank', N'121441612', N'post', NULL UNION ALL
SELECT N'hidde60', N'rabobank', N'121441625', N'post', NULL UNION ALL
SELECT N'bigbadwolfrecordstore', N'rabobank', N'121441638', N'post', NULL UNION ALL
SELECT N'hannahpannah', N'rabobank', N'121441651', N'post', NULL UNION ALL
SELECT N'siwime', N'rabobank', N'121441664', N'post', NULL UNION ALL
SELECT N'matchbox139', N'rabobank', N'121441677', N'post', NULL UNION ALL
SELECT N'schuhmode-hemp_de', N'rabobank', N'121441690', N'post', NULL UNION ALL
SELECT N'fun2sport', N'rabobank', N'121441703', N'post', NULL UNION ALL
SELECT N'marius0703', N'rabobank', N'121441716', N'post', NULL UNION ALL
SELECT N'euground', N'rabobank', N'121441729', N'post', NULL UNION ALL
SELECT N'pinbox', N'rabobank', N'121441742', N'post', NULL UNION ALL
SELECT N'willywichtig100', N'rabobank', N'121441755', N'post', NULL UNION ALL
SELECT N'kfc15ckf', N'rabobank', N'121441768', N'post', NULL UNION ALL
SELECT N'davebowman28', N'rabobank', N'121441781', N'post', NULL UNION ALL
SELECT N'matandjanesknobsandknockers', N'rabobank', N'121441794', N'post', NULL UNION ALL
SELECT N'piet812', N'rabobank', N'121441807', N'post', NULL UNION ALL
SELECT N'united-collectors', N'rabobank', N'121441820', N'post', NULL UNION ALL
SELECT N'corpogs', N'rabobank', N'121441833', N'post', NULL UNION ALL
SELECT N'sammlershop-dresden', N'rabobank', N'121441846', N'post', NULL UNION ALL
SELECT N'wholesalecomics', N'rabobank', N'121441859', N'post', NULL UNION ALL
SELECT N'penmenk', N'rabobank', N'121441872', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 8.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'metaplay.lt', N'rabobank', N'121441885', N'post', NULL UNION ALL
SELECT N'hoorne327', N'rabobank', N'121441898', N'post', NULL UNION ALL
SELECT N'amsterdam-centraal', N'rabobank', N'121441911', N'post', NULL UNION ALL
SELECT N'schaduw_nl', N'rabobank', N'121441924', N'post', NULL UNION ALL
SELECT N'patrick_boek', N'rabobank', N'121441937', N'post', NULL UNION ALL
SELECT N'heavychevy0_0', N'rabobank', N'121441950', N'post', NULL UNION ALL
SELECT N'barbarinio', N'rabobank', N'121441963', N'post', NULL UNION ALL
SELECT N'bakhuisdennis', N'rabobank', N'121441976', N'post', NULL UNION ALL
SELECT N'rudebaker55', N'rabobank', N'121441989', N'post', NULL UNION ALL
SELECT N'2259ron', N'rabobank', N'121442002', N'post', NULL UNION ALL
SELECT N'hurley6128', N'rabobank', N'121442015', N'post', NULL UNION ALL
SELECT N'thornogson1', N'rabobank', N'121442028', N'post', NULL UNION ALL
SELECT N'miriam8248', N'rabobank', N'121442041', N'post', NULL UNION ALL
SELECT N'meesterarend', N'rabobank', N'121442054', N'post', NULL UNION ALL
SELECT N'antiquariat-hesse', N'rabobank', N'121442067', N'post', NULL UNION ALL
SELECT N'boldbonobo', N'rabobank', N'121442080', N'post', NULL UNION ALL
SELECT N'hamsterklf76', N'rabobank', N'121442093', N'post', NULL UNION ALL
SELECT N'limmerix', N'rabobank', N'121442106', N'post', NULL UNION ALL
SELECT N'royo-products', N'rabobank', N'121442119', N'post', NULL UNION ALL
SELECT N'fillempie', N'rabobank', N'121442132', N'post', NULL UNION ALL
SELECT N'maarten2013', N'rabobank', N'121442145', N'post', NULL UNION ALL
SELECT N'losyfindings', N'rabobank', N'121442158', N'post', NULL UNION ALL
SELECT N'kawinglo', N'rabobank', N'121442171', N'post', NULL UNION ALL
SELECT N'chikira', N'rabobank', N'121442184', N'post', NULL UNION ALL
SELECT N'fantasmic111', N'rabobank', N'121442197', N'post', NULL UNION ALL
SELECT N'lookatmoos', N'rabobank', N'121442210', N'post', NULL UNION ALL
SELECT N'filmangel2407', N'rabobank', N'121442223', N'post', NULL UNION ALL
SELECT N'alkbart', N'rabobank', N'121442236', N'post', NULL UNION ALL
SELECT N'schnickschnack24', N'rabobank', N'121442249', N'post', NULL UNION ALL
SELECT N'mvdk1970', N'rabobank', N'121442262', N'post', NULL UNION ALL
SELECT N'mervinernest', N'rabobank', N'121442275', N'post', NULL UNION ALL
SELECT N'mdnna', N'rabobank', N'121442288', N'post', NULL UNION ALL
SELECT N'starlight_inmymindseye_ltd', N'rabobank', N'121442301', N'post', NULL UNION ALL
SELECT N'whiskyhogshead', N'rabobank', N'121442314', N'post', NULL UNION ALL
SELECT N'bb-tradingcards', N'rabobank', N'121442327', N'post', NULL UNION ALL
SELECT N'bargincityauctions', N'rabobank', N'121442340', N'post', NULL UNION ALL
SELECT N'reklameseiten', N'rabobank', N'121442353', N'post', NULL UNION ALL
SELECT N'laluna2007', N'rabobank', N'121442366', N'post', NULL UNION ALL
SELECT N'hajhdegoede', N'rabobank', N'121442379', N'post', NULL UNION ALL
SELECT N'paulgubbels2012', N'rabobank', N'121442392', N'post', NULL UNION ALL
SELECT N'rooskat', N'rabobank', N'121442405', N'post', NULL UNION ALL
SELECT N'havok(gbb)', N'rabobank', N'121442418', N'post', NULL UNION ALL
SELECT N'quint100_0', N'rabobank', N'121442431', N'post', NULL UNION ALL
SELECT N'macknm', N'rabobank', N'121442444', N'post', NULL UNION ALL
SELECT N'crest_de', N'rabobank', N'121442457', N'post', NULL UNION ALL
SELECT N'gabi-erich', N'rabobank', N'121442470', N'post', NULL UNION ALL
SELECT N'privatedreamsheidi', N'rabobank', N'121442483', N'post', NULL UNION ALL
SELECT N'cofito', N'rabobank', N'121442496', N'post', NULL UNION ALL
SELECT N'supertwaalf', N'rabobank', N'121442509', N'post', NULL UNION ALL
SELECT N'poetry366', N'rabobank', N'121442522', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 9.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'dequintzijp', N'rabobank', N'121442535', N'post', NULL UNION ALL
SELECT N'sammlerecke', N'rabobank', N'121442548', N'post', NULL UNION ALL
SELECT N'traffic-modelcarshop', N'rabobank', N'121442561', N'post', NULL UNION ALL
SELECT N'elgu0', N'rabobank', N'121442574', N'post', NULL UNION ALL
SELECT N'mannheimerxy', N'rabobank', N'121442587', N'post', NULL UNION ALL
SELECT N'tobico1', N'rabobank', N'121442600', N'post', NULL UNION ALL
SELECT N'711games_de', N'rabobank', N'121442613', N'post', NULL UNION ALL
SELECT N'wellwesell', N'rabobank', N'121442626', N'post', NULL UNION ALL
SELECT N'robsjak', N'rabobank', N'121442639', N'post', NULL UNION ALL
SELECT N'hyperfb', N'rabobank', N'121442652', N'post', NULL UNION ALL
SELECT N'thegeminigirl-26', N'rabobank', N'121442665', N'post', NULL UNION ALL
SELECT N'cheap-modelcars-netherlands', N'rabobank', N'121442678', N'post', NULL UNION ALL
SELECT N'djeepoo', N'rabobank', N'121442691', N'post', NULL UNION ALL
SELECT N'joop-de-slinger', N'rabobank', N'121442704', N'post', NULL UNION ALL
SELECT N'tomtom762011', N'rabobank', N'121442717', N'post', NULL UNION ALL
SELECT N'plukkito', N'rabobank', N'121442730', N'post', NULL UNION ALL
SELECT N'stardustshop', N'rabobank', N'121442743', N'post', NULL UNION ALL
SELECT N'mistercolleco', N'rabobank', N'121442756', N'post', NULL UNION ALL
SELECT N'handlewithcare1969', N'rabobank', N'121442769', N'post', NULL UNION ALL
SELECT N'rappelkiste_oldenburg', N'rabobank', N'121442782', N'post', NULL UNION ALL
SELECT N'arie_safarie', N'rabobank', N'121442795', N'post', NULL UNION ALL
SELECT N'sbbsander', N'rabobank', N'121442808', N'post', NULL UNION ALL
SELECT N'akseltencard', N'rabobank', N'121442821', N'post', NULL UNION ALL
SELECT N'tessa87', N'rabobank', N'121442834', N'post', NULL UNION ALL
SELECT N'bestens2011', N'rabobank', N'121442847', N'post', NULL UNION ALL
SELECT N'shop-ak', N'rabobank', N'121442860', N'post', NULL UNION ALL
SELECT N'schatjes2', N'rabobank', N'121442873', N'post', NULL UNION ALL
SELECT N'dark22222', N'rabobank', N'121442886', N'post', NULL UNION ALL
SELECT N'sunset-load', N'rabobank', N'121442899', N'post', NULL UNION ALL
SELECT N'm.f.jaqcousteau', N'rabobank', N'121442912', N'post', NULL UNION ALL
SELECT N'dreivierling', N'rabobank', N'121442925', N'post', NULL UNION ALL
SELECT N'rudihcb', N'rabobank', N'121442938', N'post', NULL UNION ALL
SELECT N'acanthis-sax', N'rabobank', N'121442951', N'post', NULL UNION ALL
SELECT N'spellekijn', N'rabobank', N'121442964', N'post', NULL UNION ALL
SELECT N'suzysuus', N'rabobank', N'121442977', N'post', NULL UNION ALL
SELECT N'gustarija', N'rabobank', N'121442990', N'post', NULL UNION ALL
SELECT N'frankoberk', N'rabobank', N'121443003', N'post', NULL UNION ALL
SELECT N'timpjee', N'rabobank', N'121443016', N'post', NULL UNION ALL
SELECT N'tibert11', N'rabobank', N'121443029', N'post', NULL UNION ALL
SELECT N'norman_martens', N'rabobank', N'121443042', N'post', NULL UNION ALL
SELECT N'kimmy-86', N'rabobank', N'121443055', N'post', NULL UNION ALL
SELECT N'buchuniversum', N'rabobank', N'121443068', N'post', NULL UNION ALL
SELECT N'amcollectables', N'rabobank', N'121443081', N'post', NULL UNION ALL
SELECT N'loch_lomond_news', N'rabobank', N'121443094', N'post', NULL UNION ALL
SELECT N'olavmeijer', N'rabobank', N'121443107', N'post', NULL UNION ALL
SELECT N'jeefour', N'rabobank', N'121443120', N'post', NULL UNION ALL
SELECT N'hoerspiele61', N'rabobank', N'121443133', N'post', NULL UNION ALL
SELECT N'commanderbensisko', N'rabobank', N'121443146', N'post', NULL UNION ALL
SELECT N'onsjans', N'rabobank', N'121443159', N'post', NULL UNION ALL
SELECT N'cdmick', N'rabobank', N'121443172', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 10.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'playpenphilatelics', N'rabobank', N'121443185', N'post', NULL UNION ALL
SELECT N'seop2002', N'rabobank', N'121443198', N'post', NULL UNION ALL
SELECT N'elmo-books', N'rabobank', N'121443211', N'post', NULL UNION ALL
SELECT N'buchetcuriosa', N'rabobank', N'121443224', N'post', NULL UNION ALL
SELECT N'gefry2013', N'rabobank', N'121443237', N'post', NULL UNION ALL
SELECT N'holland_antiques', N'rabobank', N'121443250', N'post', NULL UNION ALL
SELECT N'jocobowi1', N'rabobank', N'121443263', N'post', NULL UNION ALL
SELECT N'3kidsvon', N'rabobank', N'121443276', N'post', NULL UNION ALL
SELECT N'press*sensation', N'rabobank', N'121443289', N'post', NULL UNION ALL
SELECT N'abctoyz', N'rabobank', N'121443302', N'post', NULL UNION ALL
SELECT N'soehei', N'rabobank', N'121443315', N'post', NULL UNION ALL
SELECT N'willemwatergeus', N'rabobank', N'121443328', N'post', NULL UNION ALL
SELECT N'armarcus', N'rabobank', N'121443341', N'post', NULL UNION ALL
SELECT N'miesrommelzolder', N'rabobank', N'121443354', N'post', NULL UNION ALL
SELECT N'tsujimotochako', N'rabobank', N'121443367', N'post', NULL UNION ALL
SELECT N'three4five', N'rabobank', N'121443380', N'post', NULL UNION ALL
SELECT N'drgaz8', N'rabobank', N'121443393', N'post', NULL UNION ALL
SELECT N'af1942badminton2008', N'rabobank', N'121443406', N'post', NULL UNION ALL
SELECT N'ansichtskartenwelt', N'rabobank', N'121443419', N'post', NULL UNION ALL
SELECT N'maartenhuismanmaarten', N'rabobank', N'121443432', N'post', NULL UNION ALL
SELECT N'ronaldhoogeveen', N'rabobank', N'121443445', N'post', NULL UNION ALL
SELECT N'legrix23', N'rabobank', N'121443458', N'post', NULL UNION ALL
SELECT N'bosanova', N'rabobank', N'121443471', N'post', NULL UNION ALL
SELECT N'pimrengers', N'rabobank', N'121443484', N'post', NULL UNION ALL
SELECT N'itze86', N'rabobank', N'121443497', N'post', NULL UNION ALL
SELECT N'janneke160', N'rabobank', N'121443510', N'post', NULL UNION ALL
SELECT N'portenge123', N'rabobank', N'121443523', N'post', NULL UNION ALL
SELECT N'crown_collectables_uk', N'rabobank', N'121443536', N'post', NULL UNION ALL
SELECT N'advos', N'rabobank', N'121443549', N'post', NULL UNION ALL
SELECT N'j_meester', N'rabobank', N'121443562', N'post', NULL UNION ALL
SELECT N'ben6-44', N'rabobank', N'121443575', N'post', NULL UNION ALL
SELECT N'postzegelduifje', N'rabobank', N'121443588', N'post', NULL UNION ALL
SELECT N'doomng', N'rabobank', N'121443601', N'post', NULL UNION ALL
SELECT N'miketarrant', N'rabobank', N'121443614', N'post', NULL UNION ALL
SELECT N'palmboom2010', N'rabobank', N'121443627', N'post', NULL UNION ALL
SELECT N'jukagamesde', N'rabobank', N'121443640', N'post', NULL UNION ALL
SELECT N'arc-en-ciel81', N'rabobank', N'121443653', N'post', NULL UNION ALL
SELECT N'rijvako', N'rabobank', N'121443666', N'post', NULL UNION ALL
SELECT N'whantik', N'rabobank', N'121443679', N'post', NULL UNION ALL
SELECT N'bmwe39_m5', N'rabobank', N'121443692', N'post', NULL UNION ALL
SELECT N'madpigeonring', N'rabobank', N'121443705', N'post', NULL UNION ALL
SELECT N'antoine49', N'rabobank', N'121443718', N'post', NULL UNION ALL
SELECT N'bernd73434', N'rabobank', N'121443731', N'post', NULL UNION ALL
SELECT N'brendhold', N'rabobank', N'121443744', N'post', NULL UNION ALL
SELECT N'peter2782', N'rabobank', N'121443757', N'post', NULL UNION ALL
SELECT N'fietsracen', N'rabobank', N'121443770', N'post', NULL UNION ALL
SELECT N'kujdam', N'rabobank', N'121443783', N'post', NULL UNION ALL
SELECT N'keesstore', N'rabobank', N'121443796', N'post', NULL UNION ALL
SELECT N'et7jamo', N'rabobank', N'121443809', N'post', NULL UNION ALL
SELECT N'shop-am-wald', N'rabobank', N'121443822', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 11.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'www.ansichtskartenversand.com', N'rabobank', N'121443835', N'post', NULL UNION ALL
SELECT N'de-boekelaar', N'rabobank', N'121443848', N'post', NULL UNION ALL
SELECT N'knuffie160', N'rabobank', N'121443861', N'post', NULL UNION ALL
SELECT N'vliet1948', N'rabobank', N'121443874', N'post', NULL UNION ALL
SELECT N'happy_homeland', N'rabobank', N'121443887', N'post', NULL UNION ALL
SELECT N'kwartetanna', N'rabobank', N'121443900', N'post', NULL UNION ALL
SELECT N'korporaalfrans', N'rabobank', N'121443913', N'post', NULL UNION ALL
SELECT N'alsaret$genny99', N'rabobank', N'121443926', N'post', NULL UNION ALL
SELECT N'maranty', N'rabobank', N'121443939', N'post', NULL UNION ALL
SELECT N'micoom', N'rabobank', N'121443952', N'post', NULL UNION ALL
SELECT N'peter061965', N'rabobank', N'121443965', N'post', NULL UNION ALL
SELECT N'woudstra2004', N'rabobank', N'121443978', N'post', NULL UNION ALL
SELECT N'msteentjes', N'rabobank', N'121443991', N'post', NULL UNION ALL
SELECT N'buecher-arche', N'rabobank', N'121444004', N'post', NULL UNION ALL
SELECT N'thebikebarnltd', N'rabobank', N'121444017', N'post', NULL UNION ALL
SELECT N'5mcd', N'rabobank', N'121444030', N'post', NULL UNION ALL
SELECT N'kathie394', N'rabobank', N'121444043', N'post', NULL UNION ALL
SELECT N'hautehorlogeriebyme', N'rabobank', N'121444056', N'post', NULL UNION ALL
SELECT N'cdshopholland2', N'rabobank', N'121444069', N'post', NULL UNION ALL
SELECT N'sck777x', N'rabobank', N'121444082', N'post', NULL UNION ALL
SELECT N'magictimepuntoit', N'rabobank', N'121444095', N'post', NULL UNION ALL
SELECT N'atelier-tres-casas', N'rabobank', N'121444108', N'post', NULL UNION ALL
SELECT N'jopiecla', N'rabobank', N'121444121', N'post', NULL UNION ALL
SELECT N'startrekkie1', N'rabobank', N'121444134', N'post', NULL UNION ALL
SELECT N'gelenneke', N'rabobank', N'121444147', N'post', NULL UNION ALL
SELECT N'*spielzeug-sammelsurium*', N'rabobank', N'121444160', N'post', NULL UNION ALL
SELECT N'12-buy', N'rabobank', N'121444173', N'post', NULL UNION ALL
SELECT N'fritz-von-tarlenheim', N'rabobank', N'121444186', N'post', NULL UNION ALL
SELECT N'mina-eleberg', N'rabobank', N'121444199', N'post', NULL UNION ALL
SELECT N'2nstra', N'rabobank', N'121444212', N'post', NULL UNION ALL
SELECT N'shawcrossandson', N'rabobank', N'121444225', N'post', NULL UNION ALL
SELECT N'highlights4you', N'rabobank', N'121444238', N'post', NULL UNION ALL
SELECT N'fantasticstore', N'rabobank', N'121444251', N'post', NULL UNION ALL
SELECT N'gamepromos', N'rabobank', N'121444264', N'post', NULL UNION ALL
SELECT N'velojb1', N'rabobank', N'121444277', N'post', NULL UNION ALL
SELECT N'shouldbeworking74', N'rabobank', N'121444290', N'post', NULL UNION ALL
SELECT N'zondvloed', N'rabobank', N'121444303', N'post', NULL UNION ALL
SELECT N'mezzalunarosso', N'rabobank', N'121444316', N'post', NULL UNION ALL
SELECT N'dannygemma', N'rabobank', N'121444329', N'post', NULL UNION ALL
SELECT N'vecchiomondo', N'rabobank', N'121444342', N'post', NULL UNION ALL
SELECT N'33blauw2005', N'rabobank', N'121444355', N'post', NULL UNION ALL
SELECT N'bintphotobooks', N'rabobank', N'121444368', N'post', NULL UNION ALL
SELECT N'greenman7832', N'rabobank', N'121444381', N'post', NULL UNION ALL
SELECT N'yordib', N'rabobank', N'121444394', N'post', NULL UNION ALL
SELECT N'satoristones', N'rabobank', N'121444407', N'post', NULL UNION ALL
SELECT N'roberthuizen', N'rabobank', N'121444420', N'post', NULL UNION ALL
SELECT N'romsy78ms', N'rabobank', N'121444433', N'post', NULL UNION ALL
SELECT N'hannah11zoe', N'rabobank', N'121444446', N'post', NULL UNION ALL
SELECT N'spanje35', N'rabobank', N'121444459', N'post', NULL UNION ALL
SELECT N'ranijkamp', N'rabobank', N'121444472', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 12.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'eibihamster', N'rabobank', N'121444485', N'post', NULL UNION ALL
SELECT N'oban4me', N'rabobank', N'121444498', N'post', NULL UNION ALL
SELECT N'jellevera', N'rabobank', N'121444511', N'post', NULL UNION ALL
SELECT N'arsbkat', N'rabobank', N'121444524', N'post', NULL UNION ALL
SELECT N'cmbodyjewellery', N'rabobank', N'121444537', N'post', NULL UNION ALL
SELECT N'robertpeter_nl', N'rabobank', N'121444550', N'post', NULL UNION ALL
SELECT N'waterwoman54', N'rabobank', N'121444563', N'post', NULL UNION ALL
SELECT N'odyssey_2002', N'rabobank', N'121444576', N'post', NULL UNION ALL
SELECT N'elbenwald', N'rabobank', N'121444589', N'post', NULL UNION ALL
SELECT N'comicladen-kollektiv', N'rabobank', N'121444602', N'post', NULL UNION ALL
SELECT N'scrollbeads', N'rabobank', N'121444615', N'post', NULL UNION ALL
SELECT N'dutch_picker', N'rabobank', N'121444628', N'post', NULL UNION ALL
SELECT N'christiaan79', N'rabobank', N'121444641', N'post', NULL UNION ALL
SELECT N'mloesh85', N'rabobank', N'121444654', N'post', NULL UNION ALL
SELECT N'hm-verzamel', N'rabobank', N'121444667', N'post', NULL UNION ALL
SELECT N'hopshoof', N'rabobank', N'121444680', N'post', NULL UNION ALL
SELECT N'2013annelies', N'rabobank', N'121444693', N'post', NULL UNION ALL
SELECT N'welt-der-weisheit', N'rabobank', N'121444706', N'post', NULL UNION ALL
SELECT N'netwerk', N'rabobank', N'121444719', N'post', NULL UNION ALL
SELECT N'dutchmanshop', N'rabobank', N'121444732', N'post', NULL UNION ALL
SELECT N'droomtijd', N'rabobank', N'121444745', N'post', NULL UNION ALL
SELECT N'dr_retro', N'rabobank', N'121444758', N'post', NULL UNION ALL
SELECT N'ansichtskartenpool', N'rabobank', N'121444771', N'post', NULL UNION ALL
SELECT N'a3aan_143', N'rabobank', N'121444784', N'post', NULL UNION ALL
SELECT N'catlady-r', N'rabobank', N'121444797', N'post', NULL UNION ALL
SELECT N'het_kamertje', N'rabobank', N'121444810', N'post', NULL UNION ALL
SELECT N'unerd', N'rabobank', N'121444823', N'post', NULL UNION ALL
SELECT N'maurice_1971_f1', N'rabobank', N'121444836', N'post', NULL UNION ALL
SELECT N'jlovie', N'rabobank', N'121444849', N'post', NULL UNION ALL
SELECT N'abf2001', N'rabobank', N'121444862', N'post', NULL UNION ALL
SELECT N'misseypootle', N'rabobank', N'121444875', N'post', NULL UNION ALL
SELECT N'robertus', N'rabobank', N'121444888', N'post', NULL UNION ALL
SELECT N'pmvs', N'rabobank', N'121444901', N'post', NULL UNION ALL
SELECT N'chris4780', N'rabobank', N'121444914', N'post', NULL UNION ALL
SELECT N'boonsart', N'rabobank', N'121444927', N'post', NULL UNION ALL
SELECT N'12-sold', N'rabobank', N'121444940', N'post', NULL UNION ALL
SELECT N'airsoftadventure', N'rabobank', N'121444953', N'post', NULL UNION ALL
SELECT N'tineke3207', N'rabobank', N'121444966', N'post', NULL UNION ALL
SELECT N'hajogeur', N'rabobank', N'121444979', N'post', NULL UNION ALL
SELECT N'*dudua*', N'rabobank', N'121444992', N'post', NULL UNION ALL
SELECT N'rodemustang', N'rabobank', N'121445005', N'post', NULL UNION ALL
SELECT N'vendicum', N'rabobank', N'121445018', N'post', NULL UNION ALL
SELECT N'fiftyschans2008', N'rabobank', N'121445031', N'post', NULL UNION ALL
SELECT N'olorin1976', N'rabobank', N'121445044', N'post', NULL UNION ALL
SELECT N'archetype77', N'rabobank', N'121445057', N'post', NULL UNION ALL
SELECT N'stamp-nl', N'rabobank', N'121445070', N'post', NULL UNION ALL
SELECT N'ans2post', N'rabobank', N'121445083', N'post', NULL UNION ALL
SELECT N'harlies16', N'rabobank', N'121445096', N'post', NULL UNION ALL
SELECT N'hondmuis', N'rabobank', N'121445109', N'post', NULL UNION ALL
SELECT N'big*jerry', N'rabobank', N'121445122', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 13.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Verkoper]([Gebruikersnaam], [Bank], [Bankrekening], [Controleoptie], [Creditcard])
SELECT N'markligtvoet', N'rabobank', N'121445135', N'post', NULL UNION ALL
SELECT N'verzamelaarsmarkt', N'rabobank', N'121445148', N'post', NULL UNION ALL
SELECT N'doesje246', N'rabobank', N'121445161', N'post', NULL UNION ALL
SELECT N'richbuddy40', N'rabobank', N'121445174', N'post', NULL UNION ALL
SELECT N'starlite_shopping_mall', N'rabobank', N'121445187', N'post', NULL UNION ALL
SELECT N'findichgut1188', N'rabobank', N'121445200', N'post', NULL UNION ALL
SELECT N'postcards-stamps', N'rabobank', N'121445213', N'post', NULL UNION ALL
SELECT N'kongopfau62', N'rabobank', N'121445226', N'post', NULL UNION ALL
SELECT N'starroidraider78', N'rabobank', N'121445239', N'post', NULL UNION ALL
SELECT N'online-collector-shop', N'rabobank', N'121445252', N'post', NULL UNION ALL
SELECT N'kruiwagen4', N'rabobank', N'121445265', N'post', NULL UNION ALL
SELECT N'kidneypie71', N'rabobank', N'121445278', N'post', NULL UNION ALL
SELECT N't.vanadrichem', N'rabobank', N'121445291', N'post', NULL UNION ALL
SELECT N'henks1314', N'rabobank', N'121445304', N'post', NULL UNION ALL
SELECT N'buecherfuchs01', N'rabobank', N'121445317', N'post', NULL UNION ALL
SELECT N'vaarkoning', N'rabobank', N'121445330', N'post', NULL UNION ALL
SELECT N'zanu54', N'rabobank', N'121445343', N'post', NULL UNION ALL
SELECT N'redtrax', N'rabobank', N'121445356', N'post', NULL UNION ALL
SELECT N'pascaltf', N'rabobank', N'121445369', N'post', NULL UNION ALL
SELECT N'collectura', N'rabobank', N'121445382', N'post', NULL UNION ALL
SELECT N'doubleu80', N'rabobank', N'121445395', N'post', NULL UNION ALL
SELECT N'pamysven', N'rabobank', N'121445408', N'post', NULL UNION ALL
SELECT N'sanmar-2008', N'rabobank', N'121445421', N'post', NULL UNION ALL
SELECT N'tintinonly', N'rabobank', N'121445434', N'post', NULL
COMMIT;
RAISERROR (N'[dbo].[Verkoper]: Insert Batch: 14.....Done!', 10, 1) WITH NOWAIT;
GO

