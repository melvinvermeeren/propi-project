--------------------------------------
--      © 2014 Het Projectteam.     --
--                                  --
-- Marvin Krechting                 --
-- Harm Wouters                     --
-- Danny Peters                     --
-- Niels Bokmans                    --
-- Jasper Bloo                      --
-- Melvin Vermeeren                 --
--------------------------------------

use iproject35

insert into Vraag (Vraag) values
	('Kleur van mijn hond?'),
	('Kleur van mijn kat?'),
	('Kleur van mijn konijn?'),
	('Wat is de naam van mijn moeder?'),
	('Wat is de derde naam van mijn vader?'),
	('Wat is mijn lievelingskleur?'),
	('Wat is wit en staat in de kast?'),
	('Waarvan krijg ik de mingels?')
go

insert into Gebruiker (Gebruikersnaam, Voornaam, Achternaam, Adresregel1, Adresregel2, Postcode, Plaatsnaam, Land, GeboorteDag, Emailadres, Wachtwoord, Vraagnummer, Antwoord, IsVerkoper, IsBanned)
values
	('barry',
	 'Barry',
	 'Pooter',
	 'Langstraat 7',
	 '',
	 '2151DE',
	 'Nijmegen',
	 'Nederland',
	 '1980-12-02',
	 'barry@gmail.com',
	 'b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86',
	 -- password: password
	 '1',
	 '0',
	 'True',
	 'False'),

	('arie',
	 'Arie',
	 'Janssen',
	 'Hoogeind 3',
	 '',
	 '1248AD',
	 'Arnhem',
	 'Nederland',
	 '1985-06-06',
	 'ajanssen@gmail.com',
	 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec',
	 -- password: password
	 '2',
	 '0',
	 'True',
	 'False'),

	('kees',
	 'Kees',
	 'Tijssen',
	 'Schoolstraat 23',
	 '',
	 '9621KF',
	 'Amsterdam',
	 'Nederland',
	 '1986-08-23',
	 'keesje234@gmail.com',
	 -- password: janssen123
	 'c8a98bb1ee7078f655b2e42bad4345dbd58de2ff690457dfc05251d8213a1f7e5a033c80ed77d6f198401b42144d1443f525fd49f6636defef3e1555f310335c',
	 '3',
	 '0',
	 'False',
	 'False'),

	('karel',
	 'Karel',
	 'van Zeeuwland',
	 'Kerkstraat 34',
	 '',
	 '4568ET',
	 'Rotterdam',
	 'Nederland',
	 '1991-11-02',
	 'karelmetdeu@hotmail.com',
	 -- password: karelisgoed
	 'c5c4ecc5930adb588fa52909865dc7276e42429b853d01bac67923796e5c5ea3d04da8fb163705174ee665a7b80d35bb95ea886d15c3a58725e2d1106f118b6c',
	 '4',
	 '0',
	 'True',
	 'False'),

	('bert',
	 'Bert',
	 'van Haal',
	 'Lage poortstraat 323',
	 '',
	 '1469OP',
	 'Groningen',
	 'Nederland',
	 '1976-01-29',
	 'bertzonderernie@aol.com',
	 -- password: bertaandehaal
	 '05d62d5b7d5935b9a5a7e0f3c14ab59affe71ed044f495a10ab258ae9695d1c7fd863ef172f8edae2e060ec2b3ea15d583893e033f75a6831056b492b9e54112',
	 '5',
	 '0',
	 'True',
	 'False'),

	('freek',
	 'Freek',
	 'Schuurmans',
	 'Bredeweg 32',
	 '',
	 '8547AQ',
	 'Utrecht',
	 'Nederland',
	 '1992-03-08',
	 'freekieschuurtje@outlook.com',
	 -- password: nimdabew
	 'ce84e7875c73a07fcadd04dee1e0d2236623bb20a74ee0916d6b747ae0977eb422ab3c9c336dc4cc72a36755c03c31c9476649b2e6a69128ed899c8ba529f787',
	 '6',
	 '0',
	 'False',
	 'False'),

	('hans',
	 'Hans',
	 'Vissers',
	 'Wortelstraat 63',
	 '',
	 '7831QR',
	 'Maastricht',
	 'Nederland',
	 '1987-07-07',
	 'meneervisser@hotmail.com',
	 -- password: sretcarahc
	 '8440b7fda575e3af76693d2e45db85d8b67c096bf907ba78ea9f9db8b8837e40a6088a283a7ecd1ad38f2d1375bb71a49c5bc86445a6dc616868cbd259475c6d',
	 '7',
	 '0',
	 'True',
	 'False'),

	('annie',
	 'Annie',
	 'Mingels',
	 'Mingelweg 4',
	 '',
	 '2647DT',
	 'Heerlen',
	 'Nederland',
	 '1990-09-15',
	 'mingels@gmail.com',
	 -- password: drowssapgnikrow
	 '0a4a8c3a1ff742e8a1742b6c4492f9e6d9663444c42ad53c629837ae52e8b136bf1a4005c29b3a943bc69ea49e482ef893b2c002e8aa9d11796dc2a706273eaf',
	 '8',
	 '0',
	 'True',
	 'False')
go

insert into Verkoper (Gebruikersnaam, Bank, Bankrekening, Controleoptie, Creditcard) values
	('barry', 'rabobank', '148693214', 'post', null),
	('arie', 'rabobank', '963254445', 'post', null),
	('kees', 'sns', '253621145', 'post', null),
	('karel', 'sns', '324571124', 'post', null),
	('bert', 'ing', '257136621', 'post', null),
	('freek', 'ing', '412563321', 'post', null),
	('hans', 'rabobank', '129632282', 'post', null),
	('annie', 'rabobank', '327630014', 'post', null)
go


insert into Gebruikerstelefoon (Gebruikersnaam, Volgnummer, Telefoonnummer) values
	('barry', '1', '0685362154'),
	('arie', '1', '0632896532'),
	('kees', '1', '0639896525'),
	('karel', '1', '0698632547'),
	('bert', '1', '0636987225'),
	('freek', '1', '0614253698'),
	('hans', '1', '0687453621'),
	('annie', '1', '0614254174')
go

set identity_insert Rubriek on
insert into Rubriek (Rubrieknummer, Rubrieknaam, Volgnummer, OuderRubrieknummer) values
	(1, 'Auto''s, boten en motoren', 1, null),
	(23, 'auto''s', 1, 1),
	(24, 'Aanhangers', 2, 1),
	(76, 'Overige', 13, 1),
	(2, 'Baby', 2, null),
	(45, 'Babyverzorging', 1, 2),
	(238, 'Overige', 3, 45),

	(16, 'Muziek en instrumenten', 16, null),
	(152, 'Muziekinstrumenten', 1, 16),
	(412, 'Gitaren en versterkers', 1, 152),
	(416, 'Akoestische Gitaren', 1, 412)
set identity_insert Rubriek off
go

insert into Voorwerp (Titel, Beschrijving, Startprijs, Betalingswijze, Betalingsinstructie, Plaatsnaam,
                      Land, Looptijd, LooptijdBeginDag, LooptijdBeginTijdstip, Verzendkosten,
                      Verzendinstructies, Verkoper, Koper, LooptijdEindeDag, LooptijdEindeTijdstip,
                      isVeilingGesloten, Verkoopprijs) values
	('fietsje',
	 'ja gewoon goede fiets',
	 '23.00',
	 'contant',
	 'brengen',
	 'Nijmegen',
	 'Nederland',
	 '8',
	 '2014-05-01',
	 '23:00:00',
	 '5.00',
	 'TNT Post',
	 'barry',
	 null,
	 '2014-05-08',
	 '23:00:00',
	 'False',
	 '50.00'),

	('Mercedes SLS',
	 'best snelle bak',
	 '5200.00',
	 'bankoverschrijving',
	 'vooruitbetaling',
	 'Arnhem',
	 'Nederland',
	 '8',
	 '2014-05-12',
	 '15:00:00',
	 '0.00',
	 'Ophalen',
	 'arie',
	 null,
	 '2014-05-20',
	 '15:00:00',
	 'False',
	 '8000.00'),

	('Bokiwoki Combi Oven',
	 'Voor al uw pizzas',
	 '15.00',
	 'bankoverschrijving',
	 'vooruitbetaling',
	 'Heerlen',
	 'Nederland',
	 '8',
	 '2014-05-12',
	 '13:00:00',
	 '5.00',
	 'TNT Post',
	 'annie',
	 null,
	 '2014-05-12',
	 '13:00:00',
	 'False',
	 '30.00'),

	('Gitaar',
	 'Prima beginners gitaar',
	 '10.00',
	 'contant',
	 'brengen',
	 'Maastricht',
	 'Nederland',
	 '8',
	 '2014-05-02',
	 '16:00:00',
	 '0.00',
	 'Ophalen',
	 'hans',
	 null,
	 '2014-05-10',
	 '16:00:00',
	 'False',
	 '20.00')
go

insert into VoorwerpInRubriek (Voorwerpnummer, Rubriek) values
	('1', '238'),
	('2', '23'),
	('3', '238'),
	('4', '412')
go

insert into Bestand (Voorwerp, Bestandsnaam) values
	('1', 'fiets1.jpg'),
	('1', 'fiets2.jpg'),
	('1', 'fiets3.jpg'),
	('1', 'fiets4.jpg'),
	('2', 'merc2.jpg'),
	('3', 'oven1.jpg'),
	('3', 'oven2.jpg'),
	('3', 'oven3.jpg'),
	('3', 'oven4.jpg'),
	('4', 'gitaar2.jpg'),
	('4', 'gitaar3.jpg')
go

insert into Bod (Voorwerp, Bodbedrag, Gebruiker, BodDag, BodTijdstip) values
	('1', '24.00', 'annie', '2014-05-01', '23:10:00'),
	('1', '25.00', 'bert', '2014-05-02', '00:30:12'),
	('1', '30.00', 'annie', '2014-05-02', '05:00:00'),
	('1', '35.00', 'kees', '2014-05-02', '05:30:00'),
	('1', '45.00', 'hans', '2014-05-02', '13:00:00'),

	('2', '5210.00', 'bert', '2014-05-12', '15:30:00'),
	('2', '5300.00', 'freek', '2014-05-12', '20:30:00'),
	('2', '5350.00', 'hans', '2014-05-13', '13:00:00'),
	('2', '5400.00', 'annie', '2014-05-13', '16:00:00'),
	('2', '5800.00', 'bert', '2014-05-14', '15:30:00'),

	('3', '16.00', 'barry', '2014-05-12', '14:30:00'),
	('3', '18.00', 'arie', '2014-05-12', '15:02:00'),
	('3', '20.00', 'barry', '2014-05-13', '01:25:00'),
	('3', '25.00', 'hans', '2014-05-14', '17:15:00'),
	('3', '28.00', 'barry', '2014-05-15', '03:37:00'),

	('4', '12.00', 'annie', '2014-05-02', '16:48:00'),
	('4', '14.00', 'bert', '2014-05-02', '17:30:00'),
	('4', '15.00', 'annie', '2014-05-02', '23:12:00'),
	('4', '18.00', 'kees', '2014-05-03', '03:10:00'),
	('4', '23.00', 'barry', '2014-05-03', '11:43:00'),
	('4', '30.00', 'freek', '2014-05-03', '21:13:00'),
	('4', '33.00', 'karel', '2014-05-04', '15:42:00'),
	('4', '38.00', 'arie', '2014-05-05', '11:31:00')
go

insert into Feedback (Voorwerp, Gebruikersnaam, Feedbacksoort, Dag, Tijdstip, Commentaar) values
	('1',
	 'hans',
	 '+',
	 '2014-05-18',
	 '23:00:00',
	 'Man was erg aardig en reageerde snel. Fiets ziet er goed uit en alles werkt.'),

	('2',
	 'bert',
	 '-',
	 '2014-05-23',
	 '16:30:00',
	 'Auto was een vrak,
	  start niet. Roest is uit de fotos gephotoshopt.'),

	('3',
	 'barry',
	 '0',
	 '2014-06-01',
	 '12:31:00',
	 'Prima ding,
	  snelle levering. Jammer dat de oven niet schoongemaakt was. Pizza resten zaten er nog in.'),

	('4',
	 'arie',
	 '+',
	 '2014-05-15',
	 '13:30:00',
	 'Leuke gitaar, jammer dat er 1 snaar kapot ging bij het bekijken, maar ik kreeg er gratis een stem apparaat bij.')
go