--------------------------------------
--      © 2014 Het Projectteam.     --
--                                  --
-- Marvin Krechting                 --
-- Harm Wouters                     --
-- Danny Peters                     --
-- Niels Bokmans                    --
-- Jasper Bloo                      --
-- Melvin Vermeeren                 --
--------------------------------------

---------------------
-- Drop all tables --
---------------------
use iproject35

--  Bron: http://danhaywood.com/2012/07/18/drop-all-tables-in-an-ms-sql-server-database/

declare @table_schema varchar(100)
, @table_name varchar(100)
, @constraint_schema varchar(100)
, @constraint_name varchar(100)
, @cmd nvarchar(200)


--
-- drop all the constraints
--
declare constraint_cursor cursor for
	select
		CONSTRAINT_SCHEMA,
		CONSTRAINT_NAME,
		TABLE_SCHEMA,
		TABLE_NAME
	from INFORMATION_SCHEMA.TABLE_CONSTRAINTS
	where TABLE_NAME != 'sysdiagrams'
	order by CONSTRAINT_TYPE asc -- FOREIGN KEY, then PRIMARY KEY


open constraint_cursor
fetch next from constraint_cursor
into @constraint_schema, @constraint_name, @table_schema, @table_name

while @@FETCH_STATUS = 0
	begin
		select
			@cmd = 'ALTER TABLE [' + @table_schema + '].[' + @table_name + '] DROP CONSTRAINT [' + @constraint_name + ']'
		--select @cmd
		exec sp_executesql @cmd

		fetch next from constraint_cursor
		into @constraint_schema, @constraint_name, @table_schema, @table_name
	end

close constraint_cursor
deallocate constraint_cursor

--
-- drop all the tables
--
declare table_cursor cursor for
	select
		TABLE_SCHEMA,
		TABLE_NAME
	from INFORMATION_SCHEMA.TABLES
	where TABLE_NAME != 'sysdiagrams'
	      and TABLE_TYPE != 'VIEW'

open table_cursor
fetch next from table_cursor
into @table_schema, @table_name

while @@FETCH_STATUS = 0
	begin
		select
			@cmd = 'DROP TABLE [' + @table_schema + '].[' + @table_name + ']'
		--select @cmd
		exec sp_executesql @cmd


		fetch next from table_cursor
		into @table_schema, @table_name
	end

close table_cursor
deallocate table_cursor
------------------------------
-- End of DELETE all tables --
------------------------------

------------------------
-- Preparation steps. --
------------------------

use iproject35
go

set dateformat dmy -- Volgorde 'dag/maand/jaar' i.p.v. standaard 'maand/dag/jaar'.
go


--------------------
-- Create tables. --
--------------------

create table Vraag
(
	Vraagnummer integer      not null
	identity primary key,
	Vraag       varchar(256) not null
);
go

create table Gebruiker
(
	Gebruikersnaam varchar(32)  not null primary key check (len(Gebruikersnaam) >= 2),
	Voornaam       varchar(128) not null check (len(Voornaam) >= 2),
	Achternaam     varchar(128) not null check ((len(Achternaam) >= 2)),
	Adresregel1    varchar(128) not null check ((len(Adresregel1) >= 2)),
	Adresregel2    varchar(128) null check ((len(Adresregel2) >= 2) or (len(Adresregel2) = 0)),
-- Postcode: https://stackoverflow.com/questions/325041/i-need-to-store-postal-codes-in-a-database-how-big-should-the-column-be
	Postcode       varchar(10)  not null check (len(Postcode) >= 4),
	Plaatsnaam     varchar(128) not null check (len(Plaatsnaam) >= 2),
	Land           varchar(128) not null,
	GeboorteDag    date         not null,
	Emailadres     varchar(256) not null,
	Wachtwoord     char(128)    not null check (len(Wachtwoord) = 128), -- SHA512 hash
	Vraagnummer    integer      not null references Vraag (vraagnummer),
	Antwoord       varchar(32)  not null check (len(Antwoord) >= 1),
	IsVerkoper     bit          not null,
	IsBanned       bit          not null,

	check -- Check voor een geldig emailadres, aangepast van bron: http://vyaskn.tripod.com/handling_email_addresses_in_sql_server.htm.
	(
		charindex(' ', ltrim(rtrim(Emailadres))) = 0 -- Geen spaties.
		and left(ltrim(Emailadres), 1) <> '@'  -- '@' kan niet het eerste teken van een mailadres zijn.
		and right(rtrim(Emailadres), 1) <> '.' -- '.' kan niet het laatste teken van een mailadres zijn.
		and charindex('.', Emailadres, charindex('@', Emailadres)) - charindex('@', Emailadres) > 1
		-- Er moet een '.' zijn na een '@'.
		and len(ltrim(rtrim(Emailadres))) - len(replace(ltrim(rtrim(Emailadres)), '@', '')) = 1
		-- Er mag maar één '@' in een mailadres zitten.
		and charindex('.', reverse(ltrim(rtrim(Emailadres)))) >= 3 -- Domeinnaam is minimaal 2 tekens lang.
		and
		(charindex('.@', Emailadres) = 0 and charindex('..', Emailadres) = 0) -- Dingen als '.@' en '..' kunnen niet.
	)
);
go

create table Verkoper
(
	Gebruikersnaam varchar(32)  not null primary key references Gebruiker (gebruikersnaam),
	Bank           varchar(256) null,
	Bankrekening   varchar(34)  null,
-- 34 tekens is maximale lengte van IBAN, bron: https://en.wikipedia.org/wiki/International_Bank_Account_Number.
-- Amerikaanse bank nummers zijn maximaal 17 karakters lang, bron: https://stackoverflow.com/questions/1540285/united-states-banking-institution-account-number-regular-expression.
	Controleoptie  varchar(10)  not null check (Controleoptie in ('Creditcard', 'Post')),
	Creditcard     varchar(19)  null,
-- Maximale lengte van een creditcardnummer is 19 karakters, bron: http://www.creditcardchaser.com/how-long-is-a-credit-card-number/.
);
go

create table Voorwerp
(
	Voorwerpnummer        numeric(12,0)       not null
	identity primary key,
	Titel                 varchar(256)  not null,
	Beschrijving          nvarchar(max) null,
	Startprijs            numeric(9, 2) not null,
	Betalingswijze        varchar(256)  not null,
	Betalingsinstructie   varchar(256)  null,
	Plaatsnaam            varchar(128)  not null,
	Land                  varchar(128)  not null,
	Looptijd              integer       not null,
	LooptijdBeginDag      date          not null,
	LooptijdBeginTijdstip time(0)       not null,
	Verzendkosten         numeric(9, 2) null,
	Verzendinstructies    varchar(256)  null,
	Verkoper              varchar(32)   not null references Verkoper (Gebruikersnaam),
	Koper                 varchar(32)   null references Gebruiker (Gebruikersnaam),
	LooptijdEindeDag      date          not null,
	LooptijdEindeTijdstip time(0)       not null,
	IsVeilingGesloten     bit           not null,
	Verkoopprijs          numeric(9, 2) null
);
go

create table Bestand
(
	Voorwerp     numeric(12,0)      not null references Voorwerp (voorwerpnummer),
	Bestandsnaam varchar(256) not null primary key
);
go

create table Bod
(
	Voorwerp    numeric(12,0)       not null references Voorwerp (voorwerpnummer),
-- Met een lengte van 9 gebruikt het nog steeds slechts 5 bytes geheugen. http://msdn.microsoft.com/en-us/library/ms187746.aspx
	Bodbedrag   numeric(9, 2) not null,
	Gebruiker   varchar(32)   not null,
	BodDag      date          not null,
	BodTijdstip time(0)       not null,

	primary key (Voorwerp, Bodbedrag)
);
go

create table Feedback
(
	Voorwerp       numeric(12,0)      not null references Voorwerp (voorwerpnummer),
	Gebruikersnaam varchar(32)  not null,
	Feedbacksoort  char(1)      not null check (Feedbacksoort in ('-', '0', '+')),
	Dag            date         not null,
	Tijdstip       time(0)      not null,
	Commentaar     varchar(256) not null,

	primary key (Voorwerp, Gebruikersnaam, Dag, Tijdstip)
);
go

create table Gebruikerstelefoon
(
	Gebruikersnaam varchar(32) not null references Gebruiker (gebruikersnaam),
	Volgnummer     integer     not null,
	Telefoonnummer varchar(13) not null check (len(Telefoonnummer) >= 10),

	primary key (Gebruikersnaam, Volgnummer),
	unique (Gebruikersnaam, Telefoonnummer)
);
go

create table Rubriek
(
	Rubrieknummer      integer      not null
	identity primary key,
	Rubrieknaam        varchar(256) not null,
	OuderRubrieknummer integer      null, -- references Rubriek (Rubrieknummer),
	Volgnummer         integer      not null,
);
go

create table VoorwerpInRubriek
(
	Voorwerpnummer numeric(12,0) not null references Voorwerp (Voorwerpnummer),
	Rubriek        integer not null references Rubriek (Rubrieknummer),

	primary key (Voorwerpnummer, Rubriek)
);
go
