<?php
require_once('Voorwerp.php');

class VoorwerpTest extends PHPUnit_Framework_TestCase
{
	public function testRubrieken()
	{
		$result = Voorwerp::getVoorwerpInRubriek(23, 10);
		$this->assertTrue($result !== false && $result !== null);
	}
}
 